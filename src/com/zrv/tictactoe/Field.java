package com.zrv.tictactoe;

import java.util.Arrays;

public class Field {

    public char[][] gameField;

    public Field() {

        gameField = new char[3][3];

        for (int row = 0; row < gameField.length; row++) {

            Arrays.fill(gameField[row], ' ');
        }
    }

    public synchronized void printField() {

        for (int row = 0; row < gameField.length; row++) {
            for (int col = 0; col < gameField[row].length; col++) {
                System.out.print("\t" + gameField[row][col]);
                if (col == 0 || col == 1) {
                    System.out.print("|");
                }
            }
            if (row == 0 || row == 1) {
                System.out.print("\n---------------\n");
            }
        }
        System.out.println("\n");
        System.out.println("\n");
    }

    public synchronized boolean checkStep(char player, int row, int col) {

        if (gameField[row][col] != ' ') {
            return false;
        } else {
            gameField[row][col] = player;
            return true;
        }
    }

    public boolean notEnpty() {
        for (int row = 0; row < gameField.length; row++) {
            for (int col = 0; col < gameField[row].length; col++) {
                if (gameField[row][col] == ' ') {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}