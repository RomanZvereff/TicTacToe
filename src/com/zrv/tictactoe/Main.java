package com.zrv.tictactoe;

public class Main {

    public static void main(String[] args) {

        Player playerOne = new Player();
        Player playerTwo = new Player();

        new Thread(new Runnable() {
            @Override
            public void run() {
                playerOne.pcPlayerOne();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                playerTwo.pcPlayerTwo();
            }
        }).start();
    }
}